cmake_minimum_required(VERSION 3.3)
project(Edward)

set(EXECUTABLE_FILE "Edward")

if(NOT CMAKE_BUILD_TYPE)
    message(STATUS "Build type defaulting to \"Debug\"")
    set(CMAKE_BUILD_TYPE "Debug")
endif()

######################################################## Compiler Settings
set (CMAKE_CXX_STANDARD 14)

if(UNIX) # OS X included
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -Wall")
endif()

######################################################## Generation
if(NOT EXISTS "${CMAKE_SOURCE_DIR}/build/")
    execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_SOURCE_DIR}/build/)
endif()

######################################################## Defines
# Defines where all source files go.
set(SRC_DIR ${CMAKE_SOURCE_DIR}/src)

# Defines where all header files go.
set(INCLUDE_DIR ${CMAKE_SOURCE_DIR}/include)

# Defines where all dependecies go.
set(DEPS_DIR ${CMAKE_SOURCE_DIR}/deps)

# Defines where all binaries for dependecies go.
set(BIN_DIR ${CMAKE_SOURCE_DIR}/bin)

# So cmake knows where to put the executable and helper files.
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build")

# So cmake can use the FindXXX.cmake files for dependecies.
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

######################################################## Dependencies
# GLFW
add_subdirectory(${DEPS_DIR}/glfw)
include_directories(${DEPS_DIR}/glfw/include)

#PUGI
add_subdirectory(${DEPS_DIR}/pugixml/scripts)
include_directories(${DEPS_DIR}/pugixml/src)

#SOIL
add_subdirectory(${DEPS_DIR}/SOIL-master)
include_directories(${DEPS_DIR}/SOIL-master/src)

#SDL2
add_subdirectory(${DEPS_DIR}/SDL2)
include_directories(${DEPS_DIR}/SDL2/include)

######################################################## Linking
include_directories(${INCLUDE_DIR} ${BIN_DIR}/include)

# Setting files necessary for compilation.
set(SOURCE_FILES ${SRC_DIR}/main.cpp src/Hero.cpp src/Hero.h src/Game.cpp src/Game.h src/RenderUtils.h src/VectorUtils.h src/Tile.cpp src/Tile.h src/constants.h src/Map.cpp src/Map.h src/AliveObject.cpp src/AliveObject.h src/Rectangle.cpp src/Rectangle.h src/Intersection.cpp src/Intersection.h src/Menu.cpp src/Menu.h src/GameScene.cpp src/GameScene.h src/Monster.cpp src/Monster.h src/SoundUtils.h)

add_executable(${EXECUTABLE_FILE} ${SOURCE_FILES})
target_link_libraries(${EXECUTABLE_FILE} glfw ${GLFW_LIBRARIES} pugixml SOIL SDL2)