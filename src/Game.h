//
// Created by Adam Hornacek on 06/11/15.
//

#include <GLFW/glfw3.h>
#include <memory>
#include "Hero.h"
#include "Tile.h"
#include "AliveObject.h"
#include "Map.h"
#include "GameScene.h"
#include "Monster.h"
#include <string>

#ifndef EDWARD_GAME_H
#define EDWARD_GAME_H

using namespace std;

/**
 * Class that represents current level being played.
 */
class Game : public GameScene {


public:
    /**
     * Game constructor.
     * @param level Path to the .tmx file to load map.
     */
    Game(const string & level, int level_id) {
        this->level_id = level_id;
        map = make_unique<Map>(level);

        hero = make_unique<Hero>(map->tile_start_x * map->tile_width + map->tile_width / 2, map->tile_start_y * map->tile_height + map->tile_height / 2);

        loadMonsters();

        defeated_txt = SOIL_load_OGL_texture("../resources/Defeat_pic.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                             SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        victory_txt = SOIL_load_OGL_texture("../resources/Victory_pic.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                            SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        try_again_butt_txt = SOIL_load_OGL_texture("../resources/try_again_butt_hover.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                                  SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        main_menu_butt_txt = SOIL_load_OGL_texture("../resources/Main_menu_butt_hover.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                                   SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        next_level_butt_txt = SOIL_load_OGL_texture("../resources/Next_level_butt_hover.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                                    SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);

    }

public:
    /** Represents hero in the game.*/
    unique_ptr<Hero> hero;
    /** Represents map of the game.*/
    unique_ptr<Map> map;

private:
    /** Determines whether we should call update.*/
    bool paused = false;
    /** Determines if the player lost the level.*/
    bool defeated = false;
    /** Determines if the player won the level.*/
    bool victory = false;
    /** When showing defeat or victory determines which button is selected.*/
    bool left_button_selected = true;

    /** Which level we are in.*/
    int level_id;
    /** Hero tile x position.*/
    int hero_tile_at_x;
    /** Hero tile y position.*/
    int hero_tile_at_y;

    /** Defeated texture.*/
    GLuint defeated_txt;
    /** Victory texture.*/
    GLuint victory_txt;
    /** Try again button hover texture.*/
    GLuint try_again_butt_txt;
    /** Main menu button hover texture.*/
    GLuint main_menu_butt_txt;
    /** Next level button hover texture.*/
    GLuint next_level_butt_txt;

    /** Represents monsters currently in the level.*/
    vector<unique_ptr<Monster>> monsters;

    virtual void draw_impl(int width, int height) override;

    virtual void arrow_up_impl(bool pressed) override;

    virtual void arrow_down_impl(bool pressed) override;

    virtual void arrow_left_impl(bool pressed) override;

    virtual void arrow_right_impl(bool pressed) override;

    virtual void update_impl(unsigned long delta, int width, int height) override;

    virtual void enter_impl(bool pressed) override;

    /** Loads monsters from map.*/
    void loadMonsters();

public:
    /** Returns x position of hero so the camera will be focused on him.*/
    virtual float window_change_x() override {
        return hero->position.first;
    }

    /** Returns y position of hero so the camera will be focused on him.*/
    virtual float window_change_y() override {
        return hero->position.second;
    }


};


#endif //EDWARD_GAME_H
