//
// Created by Adam Hornacek on 26/03/16.
//

#include "Monster.h"
#include "RenderUtils.h"
#include "Rectangle.h"
#include "Intersection.h"
#include "constants.h"

void Monster::draw_impl() {
    if (turned_right) {
        draw_rectangle_with_center_at(position.first, position.second, width_half, height_half, texture);
    } else {
        draw_rectangle_with_center_at(position.first, position.second, width_half, height_half, texture, 1, 0, 0, 1);
    }
}

void Monster::update_impl(float delta, vector<shared_ptr<Tile>> & tiles, int tile_width, int tile_height) {
    std::pair<float, float> gravity_step = multiply_by_scalar(gravity, delta);
    velocity = add_vectors(velocity, gravity_step);
    velocity.first *= 0.9f;

    float speed_step = speed * delta;

    if (go_left) {
        turned_right = false;
        velocity.first -= speed_step;
    } else {
        turned_right = true;
        velocity.first += speed_step;
    }

    velocity = {clip(velocity.first, min_speed.first, max_speed.first), clip(velocity.second, min_speed.second, max_speed.second)};

    std::pair<float, float> velocity_step = multiply_by_scalar(velocity, delta);

    std::pair<float, float> desired_position = add_vectors(position, velocity_step);

    for (int i = 0; i < 8; i++) {
        int tile_index = INDICES[i];

        shared_ptr<Tile> tile = tiles[tile_index];
        if (tile != nullptr && tile->solid) {

            int tile_column = tile_index % 3;
            int tile_row = tile_index / 3;

            Rectangle player_rect(desired_position.first - collision_width_half,
                                  desired_position.second + collision_height_half, collision_width_half * 2, collision_height_half * 2);
            int tile_x = (int) (position.first / tile_width) + (tile_column - 1);
            int tile_y = (int) (position.second / tile_height) + 2 - tile_row;
            Rectangle tile_rect(tile_x * tile_width, tile_y * tile_height, tile_width, tile_height);

            Intersection intersection = Intersection::collision_of(player_rect, tile_rect);

            if (tile_index == 7) { //tile below hero
                if (intersection.get_height() > 0) {
                    desired_position = {desired_position.first, desired_position.second + intersection.get_height()};
                    if (velocity.second < 0) {
                        velocity.second = 0;
                    }
                }
            } else if (tile_index == 1) { //tile above hero
                if (intersection.get_height() > 0) {
                    desired_position = {desired_position.first, desired_position.second - intersection.get_height()};
                    if (velocity.second > 0) {
                        velocity.second = 0;
                    }
                }
            } else if (tile_index == 3) { //tile to the left of hero
                desired_position = {desired_position.first + intersection.get_width(), desired_position.second};
                go_left = false;
            } else if (tile_index == 5) { //tile to the right of hero
                desired_position = {desired_position.first - intersection.get_width(), desired_position.second};
                go_left = true;
            } else {
                float inter_size;
                if (intersection.get_width() > intersection.get_height()) {

                    if (tile_index > 4) {
                        inter_size = intersection.get_height();
                        if (velocity.second < 0) {
                            velocity.second = 0;
                        }
                    } else {
                        go_left = !go_left;
                        inter_size = -intersection.get_height();
                        if (velocity.second > 0) {
                            velocity.second = 0;
                        }
                    }
                    desired_position = {desired_position.first, desired_position.second + inter_size};
                } else {
                    if (tile_index == 6 || tile_index == 0) { //left side
                        if (tile_index == 0)
                            go_left = false;
                        inter_size = intersection.get_width();
                    } else {
                        if (tile_index == 2) {
                            go_left = true;
                        }
                        inter_size = -intersection.get_width();
                    }
                    desired_position = {desired_position.first + inter_size, desired_position.second};
                }
            }
        }
    }

    position = desired_position;
}

