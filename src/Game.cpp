//
// Created by Adam Hornacek on 06/11/15.
//

#include "Game.h"
#include "RenderUtils.h"
#include "Menu.h"
#include <iostream>
#include <cmath>

using namespace std;
using namespace edward;

extern unique_ptr<GameScene> game_scene;

void Game::draw_impl(int width, int height) {
    map->draw(width, height, hero_tile_at_x, hero_tile_at_y);

    for (auto& m : monsters) {
        m->draw();
    }

    hero->draw();

    if (defeated) {
        draw_rectangle_with_center_at(hero->position.first, hero->position.second, 222, 105, defeated_txt);
        if (left_button_selected) {
            draw_rectangle_with_center_at(hero->position.first - 100, hero->position.second - 45, 94, 34,
                                          try_again_butt_txt);
        } else {
            draw_rectangle_with_center_at(hero->position.first + 100, hero->position.second - 45, 94, 34,
                                          main_menu_butt_txt);
        }
    } else if (victory) {
        draw_rectangle_with_center_at(hero->position.first, hero->position.second, 222, 105, victory_txt);
        if (left_button_selected) {
            draw_rectangle_with_center_at(hero->position.first - 100, hero->position.second - 45, 94, 34,
                                          next_level_butt_txt);
        } else {
            draw_rectangle_with_center_at(hero->position.first + 100, hero->position.second - 45, 94, 34,
                                          main_menu_butt_txt);
        }
    }
}

void Game::arrow_up_impl(bool pressed) {
    hero->jump = pressed;
}

void Game::arrow_down_impl(bool pressed) {

}

void Game::arrow_left_impl(bool pressed) {
    if (defeated || victory) {
        if (pressed)
            left_button_selected = !left_button_selected;
    } else {
        hero->go_left = pressed;
    }
}

void Game::arrow_right_impl(bool pressed) {
    if (defeated || victory) {
        if (pressed)
            left_button_selected = !left_button_selected;
    } else {
        hero->go_right = pressed;
    }
}

void Game::update_impl(unsigned long delta, int width, int height) {
    if (paused)
        return;

    float delta_floating = float(delta) / 1000000000;

    for (auto m = monsters.begin(); m < monsters.end(); ) { //TODO: probably change the constant 200
        if (abs(hero->position.first - (*m)->position.first) < width / 2 + 200
            && abs(hero->position.second - (*m)->position.second) < height / 2 + 200) {
            (*m)->update(delta_floating, map->get_tiles_around((int) (*m)->position.first / map->tile_width,
                                                               (int) (*m)->position.second / map->tile_height),
                         map->tile_width, map->tile_height);
        }
        if ((*m)->position.second < 0) {
            m = monsters.erase(m);
        } else {
            ++m;
        }
    }

    hero_tile_at_x = (int) (hero->position.first / map->tile_width);
    hero_tile_at_y = (int) (hero->position.second / map->tile_height);

    hero->update(delta_floating, map->get_tiles_around(hero_tile_at_x, hero_tile_at_y), map->tile_width, map->tile_height);

    hero->resolve_collision_with_monsters(monsters);

    if (hero->position.second < 0 || !hero->alive) {
        defeated = true;
        paused = true;
    } else if (hero_tile_at_x == map->tile_end_x && hero_tile_at_y == map->tile_end_y) {
        victory = true;
        paused = true;
    }
}


void Game::enter_impl(bool pressed) {
    if(!pressed)
        return;

    if (defeated) {
        if (left_button_selected) { //try again
            hero->position.first = map->tile_start_x * map->tile_width + map->tile_width / 2;
            hero->position.second = map->tile_start_y * map->tile_height + map->tile_height / 2;
            hero->alive = true;
            hero->go_left = false;
            hero->go_right = false;
            hero->jump = false;
            hero->velocity = {0, 0};
            hero->turned_right = true;
            defeated = false;
            victory = false;
            paused = false;
            loadMonsters();
        } else { //main menu
            game_scene = make_unique<Menu>();
        }
    } else if (victory) {
        if (level_id == 3) {
            game_scene = make_unique<Menu>();
            return;
        }

        if (left_button_selected) { //next level
            if (level_id == 1) {
                game_scene = make_unique<Game>("../levels/level2.tmx", 2);
            } else if (level_id == 2) {
                game_scene = make_unique<Game>("../levels/level3.tmx", 3);
            }
        } else { //main menu
            game_scene = make_unique<Menu>();
        }
    }
}

void Game::loadMonsters() {
    monsters.clear();
    for (Monster m : map->monsters) {
        monsters.push_back(make_unique<Monster>(m));
    }
}


