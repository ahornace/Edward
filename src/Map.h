//
// Created by Adam Hornacek on 20/02/16.
//

#ifndef EDWARD_MAP_H
#define EDWARD_MAP_H

#include <string>
#include "AliveObject.h"
#include "Tile.h"
#include "Monster.h"
#include <vector>
#include <unordered_map>
#include <pugixml.hpp>

using namespace std;
using namespace pugi;

/**
 * Class that is useful for loading and drawing the map.
 */
class Map {
public:

    /**
     * Map constructor.
     * @param path Path to the .tmx file that represents map.
     */
    Map(const string & path);

    virtual ~Map() {
        for (auto texture : textures) {
            glDeleteTextures(1, &texture);
        }
        glDeleteTextures(1, &ice_enemy_txt);
        glDeleteTextures(1, &fire_enemy_txt);
        glDeleteTextures(1, &slime_enemy_txt);
    }

    /**
     * Draws map.
     * @param width Width of the window.
     * @param height Height of the window.
     * @param hero_x Hero tile x position.
     * @param hero_y Hero tile y position.
     */
    void draw(int width, int height, int hero_x, int hero_y);

    /** Start tile x for hero.*/
    int tile_start_x;
    /** Start tile y for hero.*/
    int tile_start_y;
    /** Tile width in map.*/
    int tile_width;
    /** Tile height in map.*/
    int tile_height;
    /** End tile x for hero.*/
    int tile_end_x;
    /** End tile y for hero.*/
    int tile_end_y;

    /** Represents monsters loaded from map.*/
    vector<Monster> monsters;
    /** Represents textures of tilesets loaded from .tmx file.*/
    vector<GLuint> textures;

    /**
     * Returns tiles around tile with coordinates x and y.
     * @param x x coordinate
     * @param y y coordinate*/
    vector<shared_ptr<Tile>> & get_tiles_around(int x, int y);

    /** Vector of tiles to be passed to hero or monsters for collision detection.*/
    vector<shared_ptr<Tile>> tiles_around_vec;

private:
    /** Map width in tiles.*/
    int map_width;
    /** Map height in tiles.*/
    int map_height;
    /** 2D array of tiles that represents map.*/
    vector<vector<shared_ptr<Tile>>> tile_map;
    /** All the tiles gathered from tilesets.*/
    vector<shared_ptr<Tile>> tiles;
    /** Loads monster textures.*/
    void load_monster_textures();
    /** Loads monsters that are encoded in .tmx file as objects.*/
    void load_monsters(xml_node objects);
    /** Loads tilesets from .tmx file.*/
    void load_tilesets(xml_node map);
    /** Loads map properties from .tmx file.*/
    void load_map_properties(xml_node map);
    /** Loads map from .tmx file.*/
    void load_map(xml_node map);
    /** Represents texture of ice enemy.*/
    GLuint ice_enemy_txt;
    /** Represents texture of fire enemy.*/
    GLuint fire_enemy_txt;
    /** Represents texture of slime enemy.*/
    GLuint slime_enemy_txt;

};


#endif //EDWARD_MAP_H
