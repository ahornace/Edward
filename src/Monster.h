//
// Created by Adam Hornacek on 26/03/16.
//

#ifndef EDWARD_MONSTER_H
#define EDWARD_MONSTER_H


#include <utility>
#include <OpenGL/OpenGL.h>
#include <array>
#include <vector>
#include "Tile.h"
#include "AliveObject.h"
#include "VectorUtils.h"

using namespace std;
using namespace edward;

/**
 * Class that represents monster in the game.
 */
class Monster : public AliveObject {

public:
    Monster& operator=(const Monster& other) {
        this->position = other.position;
        this->go_left = other.go_left;
        this->texture = other.texture;
        this->width_half = other.width_half;
        this->collision_width_half = other.collision_height_half;
        this->height_half = other.height_half;
        this->collision_height_half = other.collision_height_half;

        this->speed = other.speed;
        this->min_speed = other.min_speed;
        this->max_speed = other.max_speed;

        return *this;
    }

    Monster(const Monster& other ) {
        this->position = other.position;
        this->go_left = other.go_left;
        this->texture = other.texture;
        this->width_half = other.width_half;
        this->collision_width_half = other.collision_height_half;
        this->height_half = other.height_half;
        this->collision_height_half = other.collision_height_half;

        this->speed = other.speed;
        this->min_speed = other.min_speed;
        this->max_speed = other.max_speed;
    }

    /**
     * Monster constructor.
     * @param start_x x position of the monster.
     * @param start_y y position of the monster.
     * @param go_left go_left attribute
     * @param width_half Half of the width of the monster.
     * @param height_half Half of the height of the monster.
     * @param texture Texture of the monster.
     */
    Monster(int start_x, int start_y, bool go_left, int width_half, int height_half, GLuint texture) {
        velocity = {0, 0};
        position = {start_x, start_y};
        this->width_half = width_half;
        this->height_half = height_half;
        this->texture = texture;
        collision_width_half = width_half - 2;
        collision_height_half = height_half - 2;
        this->go_left = go_left;
    }

    /**
     * Monster constructor.
     * @param start_x x position of the monster.
     * @param start_y y position of the monster.
     * @param go_left go_left attribute
     * @param width_half Half of the width of the monster.
     * @param height_half Half of the height of the monster.
     * @param texture Texture of the monster.
     * @param speed Speed change of the monster.
     * @param max_speed Determines max speed of the monster.
     */
    Monster(int start_x, int start_y, bool go_left, int width_half, int height_half, GLuint texture, float speed, float max_speed) : Monster(start_x, start_y, go_left, width_half, height_half, texture) {
        this->speed = speed;
        min_speed.first = -max_speed;
        this->max_speed.first = max_speed;
    }

private:
    virtual void draw_impl() override;
    virtual void update_impl(float delta, vector<shared_ptr<Tile>> & tiles, int tile_width, int tile_height) override;


public:
    /** Determines whether monster should go to the left or to the right.*/
    bool go_left;
};


#endif //EDWARD_MONSTER_H
