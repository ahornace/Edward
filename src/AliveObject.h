//
// Created by Adam Hornacek on 20/02/16.
//

#ifndef EDWARD_ALIVE_OBJECT_H
#define EDWARD_ALIVE_OBJECT_H

#include <memory>
#include <vector>
#include "Tile.h"

using namespace std;

/**
 * Class that represents alive object, that means object that is visible and can move.
 */
class AliveObject {
public:
    virtual ~AliveObject() { }

    /**
     * Update this alive object.
     * @param delta Time that passed since previous update.
     * @param tiles Tiles around current object.
     * @param tile_width Width of the tiles.
     * @param tile_height Height of the tiles.
     */
    void update(float delta, vector<shared_ptr<Tile>> & tiles, int tile_width, int tile_height) {
        update_impl(delta, tiles, tile_width, tile_height);
    }

    /**
     * Draw this alive object.
     */
    void draw() {
        draw_impl();
    }

    /** Position x,y in the map. */
    std::pair<float, float> position;
    /** Current velocity.*/
    std::pair<float, float> velocity;

    /** Texture of the object.*/
    GLuint texture;

    /** Half of the width of the object for rendering.*/
    int width_half;
    /** Half ot the height of the object for rendering.*/
    int height_half;

    /** Half of the width of the object for computing collisions.*/
    int collision_width_half;
    /** Half of the height of the object for computing collisions.*/
    int collision_height_half;
    /** Determines if the object is turned to the right.*/
    bool turned_right = true;

private:
    virtual void draw_impl() = 0;
    virtual void update_impl(float delta, vector<shared_ptr<Tile>> & tiles, int tile_width, int tile_height) = 0;

protected:
    /** Gravity force that is applied to the object.*/
    std::pair<float, float> gravity = {0, -400};
    /** Minimum speed of the object in x,y.*/
    std::pair<float, float> min_speed = {-120, -450};
    /** Maximum speed of the object in x,y. */
    std::pair<float, float> max_speed = {120, 350};
    /** Speed force that is applied to the object when moving.*/
    float speed = 600;

    /**
     * Function that satisfies condition that n will be between lower and upper.
     * @param n Value we want to clip.
     * @param lower Minimum value we can return.
     * @param upper Maximum value we can return.
     * @return Returns lower if n is less than lower, returns upper if n is greater than upper, otherwise returns n.
     */
    inline float clip(float n, float lower, float upper) {
        return max(lower, min(n, upper));
    }

};


#endif //EDWARD_DRAWABLE_H
