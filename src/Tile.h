//
// Created by Adam Hornacek on 20/11/15.
//

#ifndef EDWARD_TILE_H
#define EDWARD_TILE_H

#include <GLFW/glfw3.h>
#include <SOIL.h>
#include <utility>

/**
 * Class that represents tile in the map.
 */
class Tile {
public:
    /**
     * Tile constructor.
     * @param start_x Left coordinate of the texture.
     * @param start_y Bottom coordinate of the texture.
     * @param end_x Right coordinate of the texture.
     * @param end_y Top coordinate of the texture.
     * @param texture Texture of the tile.
     */
    Tile(float start_x, float start_y, float end_x, float end_y, GLuint texture) :
            start_x(start_x), start_y(start_y), end_x(end_x), end_y(end_y), texture(texture) {}

public:
    /** Left coordinate of the texture.*/
    float start_x;
    /** Bottom coordinate of the texture.*/
    float start_y;
    /** Right coordinate of the texture.*/
    float end_x;
    /** Top coordinate of the texture.*/
    float end_y;
    /** Texture of the tile.*/
    GLuint texture;

    /** Determines whether the tile is solid.*/
    bool solid = false;
    /** Determines whether the tile is a trap.*/
    bool trap = false;
};


#endif //EDWARD_TILE_H
