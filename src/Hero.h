//
// Created by Adam Hornacek on 06/11/15.
//

#ifndef EDWARD_HERO_H
#define EDWARD_HERO_H

#include <GLFW/glfw3.h>
#include <SOIL.h>
#include <utility>
#include <vector>
#include <array>
#include "AliveObject.h"
#include "Tile.h"
#include "Monster.h"

using namespace std;

/**
 * Class that represents our hero.
 */
class Hero : public AliveObject {

public:
    /**
     * Hero constructor.
     * @param start_x x position of the hero.
     * @param start_y y position of the hero.
     */
    Hero(int start_x, int start_y);


    virtual ~Hero() {
        glDeleteTextures(1, &texture);
    }

private:
    /** Resolves any collisions with traps.
     * @param tiles Tiles around hero.
     * @param tile_width Width of the tiles.
     * @param tile_height Height of the tiles.
     */
    void resolveTraps(vector<shared_ptr<Tile>> & tiles, int tile_width, int tile_height);
    virtual void draw_impl() override;
    virtual void update_impl(float delta, vector<shared_ptr<Tile>> & tiles, int tile_width, int tile_height) override;

public:
    /** Determines whether arrow up is pressed and hero should jump.*/
    bool jump = false;
    /** Determines whether arrow right is pressed and hero should move.*/
    bool go_right = false;
    /** Determines whether arrow left is pressed and hero should move.*/
    bool go_left = false;
    /** Determines whether hero is alive.*/
    bool alive = true;

    /**
     * Resolves any collisions with monsters.
     * @param monsters Monsters to resolve collisions with.
     */
    void resolve_collision_with_monsters(vector<unique_ptr<Monster>> & monsters);

private:
    /** If user released arrow up and our velocity in vertical dimension is bigger than jump_cutoff then we will set it to its value.*/
    float jump_cutoff = 125;
    /** Velocity in vertical dimension that hero gets when jumped.*/
    float jump_velocity = 280;
    /** Determines whether hero is on ground.*/
    bool on_ground = false;

};


#endif //EDWARD_PLAYER_H
