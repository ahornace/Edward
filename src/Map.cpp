//
// Created by Adam Hornacek on 20/02/16.
//

#include "Map.h"
#include <iostream>

//load a map
Map::Map(const string & path) {

    xml_document doc;
    xml_parse_result result = doc.load_file(path.c_str());

    if (!result) {
        cout << "Parsing error" << endl;
    }

    xml_node map = doc.child("map");

    load_map_properties(map);
    load_tilesets(map);

    load_map(map);

    load_monsters(map.child("objectgroup"));

}

vector<shared_ptr<Tile>> & Map::get_tiles_around(int x, int y) {
    tiles_around_vec.clear();

    for (int i = 1; i > -2; i--) {
        for (int j = -1; j < 2; j++) {
            if (x + j >= 0 && x + j < map_width && y + i >= 0 && y + i < map_height) {
                tiles_around_vec.push_back(tile_map[y + i][x + j]);
            } else {
                tiles_around_vec.push_back(nullptr);
            }
        }
    }

    return tiles_around_vec;
}

void Map::draw(int width, int height, int hero_x, int hero_y) {

    int tiles_x_half = width / (tile_width * 2) + 2;
    int tiles_y_half = height / (tile_height * 2) + 2;

    int i_min = hero_y - tiles_y_half;
    int i_max = hero_y + tiles_y_half;
    int j_min = hero_x - tiles_x_half;
    int j_max = hero_x + tiles_x_half;

    if (i_min < 0) {
        i_min = 0;
    }
    if (i_max > map_height) {
        i_max = map_height;
    }
    if (j_min < 0) {
        j_min = 0;
    }
    if (j_max > map_width) {
        j_max = map_width;
    }

    int x_start = j_min * tile_width;
    int x = x_start;
    int y = i_min * tile_height;

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    if (textures.size() == 1) { //there is only one tileset, so we can do it more efficient and not bind texture multiple times

        glBindTexture(GL_TEXTURE_2D, textures[0]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 7);
        glBegin(GL_QUADS);

        for (int i = i_min; i < i_max; ++i) {
            for (int j = j_min; j < j_max; ++j) {

                if (tile_map[i][j] != nullptr) {

                    glTexCoord2f(tile_map[i][j]->end_x, tile_map[i][j]->end_y);
                    glVertex2f(x + tile_width, y + tile_height);
                    glTexCoord2f(tile_map[i][j]->end_x, tile_map[i][j]->start_y);
                    glVertex2f(x + tile_width, y);
                    glTexCoord2f(tile_map[i][j]->start_x, tile_map[i][j]->start_y);
                    glVertex2f(x, y);
                    glTexCoord2f(tile_map[i][j]->start_x, tile_map[i][j]->end_y);
                    glVertex2f(x, y + tile_height);

                }
                x += tile_width;
            }
            y += tile_height;
            x = x_start;
        }

        glEnd();
    } else { // there can be different texture for each tile

        for (int i = i_min; i < i_max; ++i) {
            for (int j = j_min; j < j_max; ++j) {

                if (tile_map[i][j] != nullptr) {
                    glBindTexture(GL_TEXTURE_2D, tile_map[i][j]->texture);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 7);
                    glBegin(GL_QUADS);

                    glTexCoord2f(tile_map[i][j]->end_x, tile_map[i][j]->end_y);
                    glVertex2f(x + tile_width, y + tile_height);
                    glTexCoord2f(tile_map[i][j]->end_x, tile_map[i][j]->start_y);
                    glVertex2f(x + tile_width, y);
                    glTexCoord2f(tile_map[i][j]->start_x, tile_map[i][j]->start_y);
                    glVertex2f(x, y);
                    glTexCoord2f(tile_map[i][j]->start_x, tile_map[i][j]->end_y);
                    glVertex2f(x, y + tile_height);

                    glEnd();

                }
                x += tile_width;
            }
            y += tile_height;
            x = x_start;
        }

    }
}


void Map::load_monster_textures() {
    ice_enemy_txt = SOIL_load_OGL_texture("../resources/Ice_enemy.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                          SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
    fire_enemy_txt = SOIL_load_OGL_texture("../resources/fire_enemy.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                           SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
    slime_enemy_txt = SOIL_load_OGL_texture("../resources/Slime.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                            SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);

}


void Map::load_monsters(xml_node objects) {
    load_monster_textures();

    for (xml_node object = objects.child("object"); object; object = object.next_sibling("object")) {
        int x = (int) stod(object.attribute("x").value());
        int y = map_height * tile_height - (int) stod(object.attribute("y").value());
        int type = stoi(object.attribute("type").value());
        bool go_left = true;
        xml_node properties = object.child("properties");
        for (xml_node property = properties.child("property"); property; property = property.next_sibling("property")) {
            string name = property.attribute("name").value();
            string value = property.attribute("value").value();
            if (name == "go_left") {
                go_left = value == "true";
            }
        }

        if (type == 0) {
            Monster m(x, y, go_left, 22, 29, fire_enemy_txt, 600, 120);
            monsters.push_back(m);
        } else if (type == 1) {
            Monster m(x, y, go_left, 30, 30, ice_enemy_txt, 400, 80);
            monsters.push_back(m);
        } else if (type == 2) {
            Monster m(x, y, go_left, 26, 28, slime_enemy_txt, 800, 160);
            monsters.push_back(m);
        }

    }
}

void Map::load_tilesets(xml_node map) {
    vector<int> solid_ids;
    vector<int> trap_ids;

    for (xml_node tileset = map.child("tileset"); tileset; tileset = tileset.next_sibling("tileset")) {

        int first_gid = stoi(tileset.attribute("firstgid").value());
        int tile_width = stoi(tileset.attribute("tilewidth").value());
        int tile_height = stoi(tileset.attribute("tileheight").value());

        xml_node image = tileset.child("image");
        string image_src = image.attribute("source").value();
        int image_width = stoi(image.attribute("width").value());
        int image_height = stoi(image.attribute("height").value());


        for (xml_node tile = tileset.child("tile"); tile; tile = tile.next_sibling("tile")) {
            xml_node properties = tile.child("properties");
            for (xml_node property = properties.child("property"); property; property = property.next_sibling("property")) {
                string name = property.attribute("name").value();
                string value = property.attribute("value").value();
                if (name == "solid") {
                    if (value == "true") {
                        solid_ids.push_back(first_gid - 1 + stoi(tile.attribute("id").value()));
                    }
                } else if (name == "trap") {
                    if (value == "true") {
                        trap_ids.push_back(first_gid - 1 + stoi(tile.attribute("id").value()));
                    }
                }
            }
        }

        GLuint texture = SOIL_load_OGL_texture(image_src.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                               SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);

        int id = first_gid;

        for (int y = image_height; y > 0; y -= tile_height) {
            for (int x = 0; x < image_width; x += tile_width) {

                shared_ptr<Tile> tile_ptr = make_shared<Tile>((float) (x) / image_width,
                                                              (float) (y - tile_height) / image_height,
                                                              (float) (x + tile_width) / image_width,
                                                              (float) (y) / image_height, texture);
                tiles.push_back(tile_ptr);
                ++id;
            }
        }

        textures.push_back(texture);
    }

    for (auto i : solid_ids) {
        tiles[i]->solid = true;
    }

    for (auto i : trap_ids) {
        tiles[i]->trap = true;
    }
}

void Map::load_map_properties(xml_node map) {
    map_width = stoi(map.attribute("width").value());
    map_height = stoi(map.attribute("height").value());
    tile_width = stoi(map.attribute("tilewidth").value());
    tile_height = stoi(map.attribute("tileheight").value());

    xml_node properties = map.child("properties");
    for (xml_node property = properties.child("property"); property; property = property.next_sibling("property")) {
        string name = property.attribute("name").value();
        string value = property.attribute("value").value();
        if (name == "start_x") {
            tile_start_x = stoi(value);
        } else if (name == "start_y") {
            tile_start_y = stoi(value);
        } else if (name == "end_x") {
            tile_end_x = stoi(value);
        } else if (name == "end_y") {
            tile_end_y = stoi(value);
        }
    }
}


void Map::load_map(pugi::xml_node map) {
    xml_node data = map.child("layer").child("data");
    int row = 0;
    int i = 0;
    vector<shared_ptr<Tile>> vec;
    for (xml_node tile = data.child("tile"); tile; tile = tile.next_sibling("tile")) {
        if (i >= map_width) {
            tile_map.push_back(vec);
            vec.clear();
            ++row;
            i = 0;
        }
        int val = stoi(tile.attribute("gid").value());
        if (val != 0) {
            vec.push_back(tiles[val - 1]);
        } else {
            vec.push_back(nullptr);
        }
        ++i;
    }
    tile_map.push_back(vec);

    std::reverse(tile_map.begin(), tile_map.end());
}
