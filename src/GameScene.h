//
// Created by Adam Hornacek on 23/03/16.
//

#ifndef EDWARD_GAMESCENE_H
#define EDWARD_GAMESCENE_H

/**
 * Class that represents GameScene in the game. Now those are Menu and Game objects.
 */
class GameScene {
public:

    virtual ~GameScene() { }

/**
     * Draws current GameScene.
     * @param width Width of the window.
     * @param height Height of the window.
     */
    void draw(int width, int height) {
        draw_impl(width, height);
    }

    /**
     * Called when arrow up is being manipulated by user.
     * @param pressed Determines whether key was pressed or released.
     */
    void arrow_up(bool pressed) {
        arrow_up_impl(pressed);
    }

    /**
     * Called when arrow down is being manipulated by user.
     * @param pressed Determines whether key was pressed or released.
     */
    void arrow_down(bool pressed) {
        arrow_down_impl(pressed);
    }

    /**
     * Called when arrow left is being manipulated by user.
     * @param pressed Determines whether key was pressed or released.
     */
    void arrow_left(bool pressed) {
        arrow_left_impl(pressed);
    }

    /**
     * Called when arrow right is being manipulated by user.
     * @param pressed Determines whether key was pressed or released.
     */
    void arrow_right(bool pressed) {
        arrow_right_impl(pressed);
    }

    /**
     * Called when enter key is being manipulated by user.
     * @param pressed Determines whether key was pressed or released.
     */
    void enter(bool pressed) {
        enter_impl(pressed);
    }

    /**
     * Update current GameScene.
     * @param delta Time passed since last update.
     * @param width Width of the window.
     * @param height Height of the window.
     */
    void update(unsigned long delta, int width, int height) {
        update_impl(delta, width, height);
    }

    /**
     * Method for moving camera in x dimension.
     * @return x coordinate to move camera to.
     */
    virtual float window_change_x() {
        return 0;
    }

    /**
     * Method for moving camera in y dimension.
     * @return y coordinate to move camera to.
     */
    virtual float window_change_y() {
        return 0;
    }

private:
    virtual void draw_impl(int width, int height) = 0;
    virtual void arrow_up_impl(bool pressed) = 0;
    virtual void arrow_down_impl(bool pressed) = 0;
    virtual void arrow_left_impl(bool pressed) = 0;
    virtual void arrow_right_impl(bool pressed) = 0;
    virtual void update_impl(unsigned long delta, int width, int height) = 0;
    virtual void enter_impl(bool pressed) = 0;
};


#endif //EDWARD_GAMESCENE_H
