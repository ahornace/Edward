//
// Created by Adam Hornacek on 23/03/16.
//

#include <chrono>
#include <SDL_audio.h>
#include "Menu.h"
#include "RenderUtils.h"
#include "Game.h"

extern GLFWwindow* window;
extern unique_ptr<GameScene> game_scene;

using namespace edward;

void Menu::draw_impl(int width, int height) {
    if (about) {
        draw_rectangle_with_center_at(0, 0, 210, 172, about_texture);
    } else if (level_pick) {
        draw_rectangle_with_center_at(0, 0, 190, 166, level_pick_txt);
        switch (selected_level) {
            case 0:
                draw_rectangle_with_center_at(-104, 6, 50, 50, level_1_hover_txt);
                break;
            case 1:
                draw_rectangle_with_center_at(0, 6, 50, 50, level_2_hover_txt);
                break;
            case 2:
                draw_rectangle_with_center_at(104, 6, 50, 50, level_3_hover_txt);
                break;
            default:
                draw_rectangle_with_center_at(0, -96, 154, 36, main_menu_button_hover_txt);
        }
    } else {
        draw_rectangle_with_center_at(0, 0, 190, 210, menu_texture);
        switch (selected) {
            default:
                draw_rectangle_with_center_at(0, 42, 154, 42, start_button_hover_txt);
                break;
            case 1:
                draw_rectangle_with_center_at(0, -46, 154, 42, about_button_hover_txt);
                break;
            case 2:
                draw_rectangle_with_center_at(0, -134, 154, 42, quit_button_hover_txt);
                break;
        }
    }
}

void Menu::arrow_up_impl(bool pressed) {
    if (about)
        return;
    if (pressed) {
        if (level_pick) {
            if (selected_level < 3) {
                selected_level = 3;
            } else {
                selected_level = 1;
            }
        } else {
            selected--;
            selected += 3;
            selected %= 3;
        }
    }
}

void Menu::arrow_down_impl(bool pressed) {
    if (about)
        return;

    if (pressed) {
        if (level_pick) {
            if (selected_level < 3) {
                selected_level = 3;
            } else {
                selected_level = 1;
            }
        } else {
            selected++;
            selected %= 3;
        }
    }
}

void Menu::arrow_left_impl(bool pressed) {
    if (level_pick && pressed) {
        if (selected_level < 3) {
            selected_level--;
            selected_level += 3;
            selected_level %= 3;
        }
    }
}

void Menu::arrow_right_impl(bool pressed) {
    if (level_pick && pressed) {
        if (selected_level < 3) {
            selected_level++;
            selected_level %= 3;
        }
    }
}

void Menu::update_impl(unsigned long delta, int width, int height) {

}

void Menu::enter_impl(bool pressed) {
    if (pressed) {
        if (level_pick) {
            switch (selected_level) {
                case 0:
                    game_scene = make_unique<Game>("../levels/level1.tmx", 1);
                    break;
                case 1:
                    game_scene = make_unique<Game>("../levels/level2.tmx", 2);
                    break;
                case 2:
                    game_scene = make_unique<Game>("../levels/level3.tmx", 3);
                    break;
                default:
                    level_pick = false;
                    selected_level = 0;
            }
        } else {
            switch (selected) {
                default:
                    level_pick = true;
                    break;
                case 1:
                    about = !about;
                    break;
                case 2:
                    SDL_CloseAudio();
                    glfwSetWindowShouldClose(window, GL_TRUE);
                    break;
            }
        }
    }
}
