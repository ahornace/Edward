//
// Created by Adam Hornacek on 23/03/16.
//

#ifndef EDWARD_MENU_H
#define EDWARD_MENU_H

#include <GLFW/glfw3.h>
#include <SOIL.h>
#include "GameScene.h"
#include <string>

using namespace std;

/**
 * Class that represents menu.
 */
class Menu : public GameScene {

public:
    Menu() {
        menu_texture = SOIL_load_OGL_texture("../resources/Main_menu2.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                             SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        about_texture = SOIL_load_OGL_texture("../resources/About.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                             SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        start_button_hover_txt = SOIL_load_OGL_texture("../resources/hover_s.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                                       SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        about_button_hover_txt = SOIL_load_OGL_texture("../resources/hover_a.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                                       SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        quit_button_hover_txt = SOIL_load_OGL_texture("../resources/hover_q.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                                      SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        level_pick_txt = SOIL_load_OGL_texture("../resources/Level_pick.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                               SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        main_menu_button_hover_txt = SOIL_load_OGL_texture("../resources/hover_main_menu.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                                           SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        level_1_hover_txt = SOIL_load_OGL_texture("../resources/hover_1.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                                  SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        level_2_hover_txt = SOIL_load_OGL_texture("../resources/hover_2.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                                  SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
        level_3_hover_txt = SOIL_load_OGL_texture("../resources/hover_3.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                                  SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
    }

    virtual ~Menu() {
        glDeleteTextures(1, &menu_texture);
        glDeleteTextures(1, &about_texture);
        glDeleteTextures(1, &start_button_hover_txt);
        glDeleteTextures(1, &about_button_hover_txt);
        glDeleteTextures(1, &quit_button_hover_txt);
        glDeleteTextures(1, &level_pick_txt);
        glDeleteTextures(1, &main_menu_button_hover_txt);
        glDeleteTextures(1, &level_1_hover_txt);
        glDeleteTextures(1, &level_2_hover_txt);
        glDeleteTextures(1, &level_3_hover_txt);
    }

private:
    GLuint menu_texture;
    GLuint about_texture;
    GLuint start_button_hover_txt;
    GLuint about_button_hover_txt;
    GLuint quit_button_hover_txt;

    GLuint level_pick_txt;
    GLuint main_menu_button_hover_txt;
    GLuint level_1_hover_txt;
    GLuint level_2_hover_txt;
    GLuint level_3_hover_txt;

    /** Determines whether we should show about section.*/
    bool about = false;
    /** Determines wheter we should show level pick section.*/
    bool level_pick = false;

    /** Determines which button in main menu is selected.*/
    int selected = 0;
    /** Determines which button is selected in level pick section.*/
    int selected_level = 0;

    virtual void draw_impl(int width, int height) override;

    virtual void arrow_up_impl(bool pressed) override;

    virtual void arrow_down_impl(bool pressed) override;

    virtual void arrow_left_impl(bool pressed) override;

    virtual void arrow_right_impl(bool pressed) override;

    virtual void update_impl(unsigned long delta, int width, int height) override;

    virtual void enter_impl(bool pressed) override;

};


#endif //EDWARD_MENU_H
