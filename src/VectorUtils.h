//
// Created by Adam Hornacek on 20/11/15.
//

#ifndef EDWARD_VECTORUTILS_H
#define EDWARD_VECTORUTILS_H

#include <utility>

namespace edward {

    /**
     * Multiplies pair by a scalar.
     * @param vector 2D vector to be multiplied
     * @param value Value by which to multiply.
     * @return Multiplied vector by value.
     */
    inline std::pair<float, float> multiply_by_scalar(std::pair<float, float> vector, float value) {
        return {vector.first * value, vector.second * value};
    }

    /**
     * Adds two pairs together
     * @param v1 First pair.
     * @param v2 Second pair.
     * @return Added two pair parameters.
     */
    inline std::pair<float, float> add_vectors(std::pair<float, float> v1, std::pair<float, float> v2) {
        return {v1.first + v2.first, v1.second + v2.second};
    }

}


#endif //EDWARD_VECTORUTILS_H
