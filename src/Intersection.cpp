//
// Created by Adam Hornacek on 11/03/16.
//

#include "Intersection.h"

Intersection Intersection::collision_of(Rectangle r1, Rectangle r2) {

    Intersection i(0, 0);

    if (r1.top_left_x < r2.top_left_x + r2.width &&
            r1.top_left_x + r1.width > r2.top_left_x &&
            r1.top_left_y > r2.top_left_y - r2.height &&
            r1.top_left_y - r1.height < r2.top_left_y) { //rectangles overlap

        if (r1.top_left_x < r2.top_left_x) {
            if (r1.top_left_x + r1.width < r2.top_left_x + r2.width) { //left side of r2 is in r1 and right side of r2 is beyond r1
                i.intersection_width = r1.top_left_x + r1.width - r2.top_left_x;
            } else { //second rectangle is included in the first
                i.intersection_width = r2.width;
            }
        } else { //r2 starts before r1
            i.intersection_width = r2.top_left_x + r2.width - r1.top_left_x;
        }

        if (r1.top_left_y > r2.top_left_y) { //top of r2 is below of top of r1
            if (r1.top_left_y - r1.height < r2.top_left_y - r2.height) { // second rectangle is in the first
                i.intersection_height = r2.height;
            } else {
                i.intersection_height = r2.top_left_y - (r1.top_left_y - r1.height);
            }
        } else {
            i.intersection_height = r1.top_left_y - (r2.top_left_y - r2.height);
        }
    }

    return i;
}
