//
// Created by Adam Hornacek on 20/02/16.
//

#ifndef EDWARD_CONSTANTS_H
#define EDWARD_CONSTANTS_H

#include <array>

/** Lag threshold so objects cannot suddenly move after lag.*/
const unsigned long LAG_THRESHOLD = 20000000;
/** Order of the tiles checking for collisions.*/
const std::array<int, 8> INDICES = {{7, 1, 3, 5, 0, 2, 6, 8}};
/** Determines whether the game should play sounds.*/
const bool SOUNDS_ENABLED = true;


#endif //EDWARD_CONSTANTS_H
