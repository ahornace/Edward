//
// Created by Adam Hornacek on 06/11/15.
//

#include "Hero.h"
#include "RenderUtils.h"
#include "VectorUtils.h"
#include "Rectangle.h"
#include "Intersection.h"
#include "constants.h"
#include <iostream>
#include <array>

using namespace std;
using namespace edward;

Hero::Hero(int start_x, int start_y) {
    velocity = {0, 0};
    position = {start_x, start_y};
    texture = SOIL_load_OGL_texture("../resources/edward.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                    SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA);
    width_half = 18;
    height_half = 26;

    collision_width_half = width_half - 2;
    collision_height_half = height_half - 2;
}


void Hero::update_impl(float delta, vector<shared_ptr<Tile>> & tiles, int tile_width, int tile_height) {
    std::pair<float, float> gravity_step = multiply_by_scalar(gravity, delta);
    velocity = add_vectors(velocity, gravity_step);
    velocity.first *= 0.9f;

    float speed_step = speed * delta;

    if (jump && on_ground) {
        velocity.second = jump_velocity;
    } else if (!jump && velocity.second > jump_cutoff) {
        velocity.second = jump_cutoff;
    }

    if (go_right && !go_left) {
        turned_right = true;
        velocity.first += speed_step;
    } else if (go_left && !go_right) {
        turned_right = false;
        velocity.first -= speed_step;
    }

    velocity = {clip(velocity.first, min_speed.first, max_speed.first), clip(velocity.second, min_speed.second, max_speed.second)};

    std::pair<float, float> velocity_step = multiply_by_scalar(velocity, delta);

    std::pair<float, float> desired_position = add_vectors(position, velocity_step);

    on_ground = false;

    for (int i = 0; i < 8; i++) {
        int tile_index = INDICES[i];

        shared_ptr<Tile> tile = tiles[tile_index];
        if (tile != nullptr && tile->solid) {

            int tile_column = tile_index % 3;
            int tile_row = tile_index / 3;

            Rectangle player_rect(desired_position.first - collision_width_half,
                                  desired_position.second + collision_height_half, collision_width_half * 2, collision_height_half * 2);
            int tile_x = (int) (position.first / tile_width) + (tile_column - 1);
            int tile_y = (int) (position.second / tile_height) + 2 - tile_row;
            Rectangle tile_rect(tile_x * tile_width, tile_y * tile_height, tile_width, tile_height);

            Intersection intersection = Intersection::collision_of(player_rect, tile_rect);

            if (tile_index == 7) { //tile below hero
                if (intersection.get_height() > 0) {
                    desired_position = {desired_position.first, desired_position.second + intersection.get_height()};
                    if (velocity.second < 0) {
                        velocity.second = 0;
                    }
                    on_ground = true;
                }
            } else if (tile_index == 1) { //tile above hero
                if (intersection.get_height() > 0) {
                    desired_position = {desired_position.first, desired_position.second - intersection.get_height()};
                    if (velocity.second > 0) {
                        velocity.second = 0;
                    }
                }
            } else if (tile_index == 3) { //tile to the left of hero
                desired_position = {desired_position.first + intersection.get_width(), desired_position.second};
            } else if (tile_index == 5) { //tile to the right of hero
                desired_position = {desired_position.first - intersection.get_width(), desired_position.second};
            } else {
                float inter_size;
                if (intersection.get_width() > intersection.get_height()) {

                    if (tile_index > 4) {
                        inter_size = intersection.get_height();
                        if (velocity.second < 0) {
                            velocity.second = 0;
                        }
                        on_ground = true;
                    } else {
                        inter_size = -intersection.get_height();
                        if (velocity.second > 0) {
                            velocity.second = 0;
                        }
                    }
                    desired_position = {desired_position.first, desired_position.second + inter_size};
                } else {
                    if (tile_index == 6 || tile_index == 0) { //left side
                        inter_size = intersection.get_width();
                    } else {
                        inter_size = -intersection.get_width();
                    }
                    desired_position = {desired_position.first + inter_size, desired_position.second};
                }
            }
        }
    }

    resolveTraps(tiles, tile_width, tile_height);
    position = desired_position;
}

void Hero::resolveTraps(vector<shared_ptr<Tile>> & tiles, int tile_width, int tile_height) {
    if (on_ground) {
        Rectangle player_rect(position.first - collision_width_half, position.second + collision_height_half,
                              collision_width_half * 2, collision_height_half * 2);

        if (tiles[4] != nullptr && tiles[4]->trap) { //we are in the trap
            alive = false;
            return;
        }

        for (int i = 0; i < 8; i++) {
            int tile_index = INDICES[i];

            shared_ptr<Tile> tile = tiles[tile_index];
            if (tile != nullptr && tile->trap) {

                int tile_column = tile_index % 3;
                int tile_row = tile_index / 3;

                int tile_x = (int) (position.first / tile_width) + (tile_column - 1);
                int tile_y = (int) (position.second / tile_height) + 2 - tile_row;
                Rectangle tile_rect(tile_x * tile_width, tile_y * tile_height, tile_width, tile_height);

                Intersection intersection = Intersection::collision_of(player_rect, tile_rect);

                if (intersection.get_height() > 0 || intersection.get_width() < 0) {
                    alive = false;
                    break;
                }
            }
        }
    }
}

void Hero::resolve_collision_with_monsters(vector<unique_ptr<Monster>> &monsters) {
    Rectangle player_rect(position.first - collision_width_half, position.second + collision_height_half,
                          collision_width_half * 2, collision_height_half * 2);

    for (auto m = monsters.begin(); m < monsters.end(); ) {
        Rectangle monster_rect((*m)->position.first - (*m)->collision_width_half,
                               (*m)->position.second + (*m)->collision_height_half,
                               (*m)->collision_width_half * 2, (*m)->collision_height_half * 2);

        Intersection intersection = Intersection::collision_of(player_rect, monster_rect);

        if (intersection.get_height() > 0 && position.second - collision_height_half / 2 > (*m)->position.second + (*m)->collision_height_half) { //I killed the monster
            m = monsters.erase(m);
            break;
        } else if (intersection.get_height() > 0 || intersection.get_width() > 0){
            alive = false;
            break;
        } else {
            ++m;
        }

    }

}

void Hero::draw_impl() {
    if (turned_right) {
        draw_rectangle_with_center_at(position.first, position.second, width_half, height_half, texture);
    } else {
        draw_rectangle_with_center_at(position.first, position.second, width_half, height_half, texture, 1, 0, 0, 1);
    }
}
