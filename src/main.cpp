#include <stdlib.h>
#include <SDL.h>
#include <iostream>
#include <GLFW/glfw3.h>
#include "Game.h"
#include "constants.h"
#include "Menu.h"
#include "SoundUtils.h"

using namespace std;

/**
 * @mainpage Edward
 * @section intro Introduction
 * Edward is a simple 2D platform game made as a school project for subject C++ at Charles University in Prague.
 * @section lib Libraries
 * In the project were used following libraries:
 * <ul>
 * <li><strong>GLFW</strong> - window and opengl support</li>
 * <li><strong>PugiXML</strong> - xml parsing support</li>
 * <li><strong>SDL2</strong> - audio support</li>
 * <li><strong>SOIL</strong> - png loading as textures</li>
 * </ul>
 */

/** Current GameScene in window.*/
unique_ptr<GameScene> game_scene;
/** Window of the game.*/
GLFWwindow* window;

/** Width of the window.*/
int width;
/** Height of the window.*/
int height;

/**
 * GLFW error callback
 */
static void error_callback(int error, const char* description)
{
    fputs(description, stderr);
}

/**
 * GLFW key callback where we call proper actions on current GameScene.
 */
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_RIGHT:
                game_scene->arrow_right(true);
                break;
            case GLFW_KEY_LEFT:
                game_scene->arrow_left(true);
                break;
            case GLFW_KEY_UP:
                game_scene->arrow_up(true);
                break;
            case GLFW_KEY_DOWN:
                game_scene->arrow_down(true);
                break;
            case GLFW_KEY_ENTER:
                game_scene->enter(true);
                break;
            default:
                //ignore
                break;
        }
    } else if (action == GLFW_RELEASE) {
        switch (key) {
            case GLFW_KEY_RIGHT:
                game_scene->arrow_right(false);
                break;
            case GLFW_KEY_LEFT:
                game_scene->arrow_left(false);
                break;
            case GLFW_KEY_UP:
                game_scene->arrow_up(false);
                break;
            case GLFW_KEY_DOWN:
                game_scene->arrow_down(false);
                break;
            case GLFW_KEY_ENTER:
                game_scene->enter(false);
                break;
            default:
                //ignore
                break;
        }
    }
}

typedef chrono::duration<unsigned long long , std::nano> nanoseconds;
typedef chrono::duration<double, typename chrono::high_resolution_clock::period> Cycle;

/**
 * Start of the rendering where we initialize camera.
 * @param window Window of the game.
 * @param game_scene Current GameScene.
 */
void start_rendering(GLFWwindow *window, unique_ptr<GameScene> & game_scene) {
    glfwGetFramebufferSize(window, &width, &height);

    int width_half = width / 2;
    int height_half = height / 2;

    glClearColor(0.404, 0.419, 0.941, 1);
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-width_half + game_scene->window_change_x(), width_half + game_scene->window_change_x(),
            -height_half + game_scene->window_change_y(), game_scene->window_change_y() + height_half, 0, 1);
}

/**
 * End of the rendering stuff.
 * @param window Window of the game.
 */
void end_rendering(GLFWwindow *window) {
    glfwSwapBuffers(window);
    glfwPollEvents();
}

/**
 * Main function of the game. Contains loop in which we update and draw GameScene.
 */
int main(int argc, char **argv) {

    if (SOUNDS_ENABLED) {
        play_music("../resources/music.wav");
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_SAMPLES, 4);

    //starting window
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
        exit(EXIT_FAILURE);
    window = glfwCreateWindow(800, 600, "Edward", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwSetKeyCallback(window, key_callback);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);


    game_scene = make_unique<Menu>();

    auto last_time = chrono::system_clock::now();

    while (!glfwWindowShouldClose(window)) {

        auto current_time = chrono::system_clock::now();

        auto delta = Cycle(current_time - last_time);

        unsigned long long delta_in_long = chrono::duration_cast<nanoseconds>(delta).count();
        game_scene->update(delta_in_long > LAG_THRESHOLD ? LAG_THRESHOLD : delta_in_long, width, height);

        start_rendering(window, game_scene);

        game_scene->draw(width, height);

        end_rendering(window);

        last_time = current_time;
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}