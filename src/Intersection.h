//
// Created by Adam Hornacek on 11/03/16.
//

#ifndef EDWARD_COLLISION_H
#define EDWARD_COLLISION_H


#include "Rectangle.h"

/**
 * This class represents collision of two rectangles and provides method for its calculation.
 */
class Intersection {


public:
    /**
     * Intersection constructor
     * @param intersection_height Height of the intersection.
     * @param intersection_width Width of the intersection.
     */
    Intersection(int intersection_height, int intersection_width) : intersection_height(intersection_height),
                                                           intersection_width(intersection_width) { }
    /**
     * Returns intersection of two rectangles.
     * @param r1 First rectangle.
     * @param r2 Second rectangle.
     * @return Intersection of rectangles given as parameters.
     */
    static Intersection collision_of(Rectangle r1, Rectangle r2);

    /**
     * @return Height of the intersection.
     */
    float inline get_height() {
        return intersection_height;
    }

    /**
     * @return Width of the intersection.
     */
    float inline get_width() {
        return intersection_width;
    }

private:
    /** Intersection height.*/
    float intersection_height;
    /** Intersection width.*/
    float intersection_width;

};


#endif //EDWARD_COLLISION_H
