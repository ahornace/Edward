//
// Created by Adam Hornacek on 06/11/15.
//

#include <GLFW/glfw3.h>

#ifndef EDWARD_RENDERUTILS_H
#define EDWARD_RENDERUTILS_H

namespace edward {

    /**
     * Renders rectangle.
     * @param x x coordinate of the center of the rendered rectangle.
     * @param y y coordinate of the center of the rendered rectangle.
     * @param dx_half Half of the width of the rectangle.
     * @param dy_half Half of the height of the rectangle.
     * @param texture Texture of the rendered rectangle.
     */
    inline void draw_rectangle_with_center_at(float x, float y, int dx_half, int dy_half, GLuint texture) {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glBindTexture(GL_TEXTURE_2D, texture);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 7);
        glBegin(GL_QUADS);

        glTexCoord2i(1, 1);
        glVertex2f(x + dx_half, y + dy_half);
        glTexCoord2i(1, 0);
        glVertex2f(x + dx_half, y - dy_half);
        glTexCoord2i(0, 0);
        glVertex2f(x - dx_half, y - dy_half);
        glTexCoord2i(0, 1);
        glVertex2f(x - dx_half, y + dy_half);

        glEnd();
    }

    /**
     * Renders rectangle.
     * @param x x coordinate of the center of the rendered rectangle.
     * @param y y coordinate of the center of the rendered rectangle.
     * @param dx_half Half of the width of the rectangle.
     * @param dy_half Half of the height of the rectangle.
     * @param texture Texture of the rendered rectangle.
     * @param start_x Left coordinate of the texture.
     * @param start_y Bottom coordinate of the texture.
     * @param end_x Right coordinate of the texture.
     * @param end_y Top coordinateof the texture.
     */
    inline void draw_rectangle_with_center_at(float x, float y, int dx_half, int dy_half, GLuint texture,
                                              float start_x, float start_y, float end_x, float end_y) {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glBindTexture(GL_TEXTURE_2D, texture);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 7);
        glBegin(GL_QUADS);

        glTexCoord2f(end_x, end_y);
        glVertex2f(x + dx_half, y + dy_half);
        glTexCoord2f(end_x, start_y);
        glVertex2f(x + dx_half, y - dy_half);
        glTexCoord2f(start_x, start_y);
        glVertex2f(x - dx_half, y - dy_half);
        glTexCoord2f(start_x, end_y);
        glVertex2f(x - dx_half, y + dy_half);

        glEnd();
    }

}


#endif //EDWARD_RENDERUTILS_H
