//
// Created by Adam Hornacek on 11/03/16.
//

#ifndef EDWARD_RECTANGLE_H
#define EDWARD_RECTANGLE_H

/**
 * Model class of the rectangle.
 */
class Rectangle {

public:
    /**
     * Rectangle constructor.
     * @param top_left_x Top left x coordinate.
     * @param tol_left_y Top left y coordinate.
     * @param width Width of the rectangle.
     * @param height Height of the rectangle.
     */
    Rectangle(float top_left_x, float top_left_y, int width, int height) : top_left_x(top_left_x),
                                                                           top_left_y(top_left_y), width(width),
                                                                           height(height) { }

public:
    /** Top left x coordinate of the rectangle.*/
    float top_left_x;
    /** Top left y coordinate of the rectangle.*/
    float top_left_y;
    /** Width of the rectangle.*/
    int width;
    /** Height of the rectangle.*/
    int height;

};


#endif //EDWARD_RECTANGLE_H
