//
// Created by Adam Hornacek on 27/03/16.
//

#ifndef EDWARD_SOUNDUTILS_H
#define EDWARD_SOUNDUTILS_H

#include <SDL_types.h>
#include <string>
#include <SDL.h>

using namespace std;

namespace edward {

    //example of playing wav file using sdl from: https://gist.githubusercontent.com/LaughingSun/e3450d28012a44182ebc/raw/b113e899aaf2a83ad8f3fae3c32522c2438b77d1/sdl2-loadwav.c

    struct AudioSpecUserdata_t {
        string file;
        Uint32 event_count;
        Uint8 *buffer;
        Uint32 buffer_len;
        Uint32 loaded_len;
    };

    static struct AudioSpecUserdata_t OpenAudio_callback_userdata;

    static void OpenAudio_callback(void *userdata, Uint8 *stream, int len) {
        struct AudioSpecUserdata_t *sdata = (struct AudioSpecUserdata_t *) userdata;
        Uint32 new_len;

        sdata->event_count++;

        if (len > 0 && sdata && sdata->buffer) {
            if ((new_len = sdata->loaded_len + len) > sdata->buffer_len) {
                len = sdata->buffer_len - sdata->loaded_len;
                new_len = sdata->buffer_len;
            }
            SDL_memcpy(stream, sdata->buffer + sdata->loaded_len,
                       len);                    // simply copy from one buffer into the other
            sdata->loaded_len = new_len;
        }
        if (sdata->loaded_len >= sdata->buffer_len) { sdata->loaded_len = 0; }
    }

    // it might be the same but the address should be different.
    static struct AudioSpecUserdata_t LoadWAV_callback_userdata;

    static void LoadWAV_callback(void *userdata, Uint8 *stream, int len) {
        struct AudioSpecUserdata_t *sdata = (struct AudioSpecUserdata_t *) userdata;
        Uint32 new_len;

        sdata->event_count++;

        if (len > 0 && sdata && sdata->buffer) {
            if ((new_len = sdata->loaded_len + len) > sdata->buffer_len) {
                len = sdata->buffer_len - sdata->loaded_len;
                new_len = sdata->buffer_len;
            }
            SDL_memcpy(stream, sdata->buffer + sdata->loaded_len,
                       len);                    // simply copy from one buffer into the other
            sdata->loaded_len = new_len;
        }
        if (sdata->loaded_len >= sdata->buffer_len) { sdata->loaded_len = 0; } // Quit = 1;
    }

    /**
     * Plays music.
     * @param file Wav file from which music to play.
     */
    inline void play_music(const string & file) {
        SDL_AudioSpec loadWAV_spec;
        SDL_AudioSpec openAudio_obtained_spec;
        Uint32 audio_len;
        Uint8* audio_buf;

        // Initialize SDL.
        if (SDL_Init(SDL_INIT_AUDIO) < 0) {
            printf("SDL_Init(SDL_INIT_AUDIO) < 0\n");
            return;
        }

        // open wav file, put specification in have, audio location in audiobuf and length in length
        loadWAV_spec.callback = LoadWAV_callback;
        loadWAV_spec.userdata = (void *)&LoadWAV_callback_userdata;
        LoadWAV_callback_userdata.file = file;
        LoadWAV_callback_userdata.loaded_len = 0;
        if( ! SDL_LoadWAV(file.c_str(), &loadWAV_spec, &audio_buf, &audio_len)) {
            printf("[SDL] Failed: SDL_LoadWAV: %s\n", SDL_GetError());
            SDL_Quit();
            return;
        }

        LoadWAV_callback_userdata.file = file;
        LoadWAV_callback_userdata.buffer = audio_buf;
        LoadWAV_callback_userdata.buffer_len = audio_len;
        LoadWAV_callback_userdata.event_count = 0;

        //open audio device
        loadWAV_spec.callback = OpenAudio_callback;
        loadWAV_spec.userdata = (void *)&OpenAudio_callback_userdata;
        OpenAudio_callback_userdata.file = file;
        OpenAudio_callback_userdata.buffer = audio_buf;
        OpenAudio_callback_userdata.buffer_len = audio_len;
        OpenAudio_callback_userdata.event_count = 0;
        if ( SDL_OpenAudio(&loadWAV_spec, &openAudio_obtained_spec) != 0 ) {
            printf("[SDL]  Couldn't open audio: %s\n", SDL_GetError());
            exit(-1);
        }

        //play audio
        SDL_PauseAudio(0);
    }

    /**
     * Pauses music.
     */
    inline void pause_music() {
        SDL_PauseAudio(1);
    }

}

#endif //EDWARD_SOUNDUTILS_H
